const http = require("http");
const dayjs = require("dayjs");

exports.handler = (event, context, callback) => {
    const now = dayjs().format("YYYY-MM-DD HH:mm:ss");
    const url = "http://holiday-cn.liuxianyu.cn/api/holiday";

    http.get(url, (data) => {
        let str = "";
        data.on("data", (chunk) => {
            str += chunk; //监听数据响应，拼接数据片段
        });
        data.on("end", () => {
            console.log(now, str.toString());
            callback();
        });
    });
};
